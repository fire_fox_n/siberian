<?php

namespace app\services;


use app\entities\Project;
use app\repositories\ProjectRepository;

class ProjectService
{
    private $projectRepo;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepo = $projectRepository;
    }

    public function create($name, $status = '10')
    {
        $project = Project::create(
            $name,
            $status
        );

        $this->projectRepo->save($project);

        return $project;
    }
}