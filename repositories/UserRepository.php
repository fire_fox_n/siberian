<?php
/**
 * Created by PhpStorm.
 * User: natali
 * Date: 03.09.17
 * Time: 21:51
 */

namespace app\repositories;


use app\entities\Profile;
use app\entities\User;
use yii\web\NotFoundHttpException;

class UserRepository
{
    public function get($id)
    {
        $id = intval($id);
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException('Пользователь не найден.');
        }

        return $user;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function getByEmail($email)
    {
        return $this->getBy(['email' => $email]);
    }

    public function getByFullName($fullName)
    {
        $arrName = explode(' ', $fullName);
        /** @var Profile $profile */
        $profile = Profile::find()->andWhere([
            'lastname' => $arrName[0],
            'firstname' => $arrName[1]
        ])->limit(1)->one();

        if (!$profile) {
            throw new NotFoundException('Такой профиль не найден.');
        }

        $user = $profile->user;

        return $user;
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        if (!$user->save()) {
            throw new \RuntimeException('Ошибка сохранения пользователя.');
        }
    }

    public function saveProfile(Profile $profile)
    {
        if (!$profile->save()) {
            throw new \RuntimeException('Ошибка сохранения профиля.');
        }
    }

    /**
     * @param array $condition
     *
     * @return User
     * @throws NotFoundHttpException
     */
    private function getBy(array $condition)
    {
        if (!$user = User::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundHttpException('Пользователь не найден.');
        }

        return $user;
    }
}