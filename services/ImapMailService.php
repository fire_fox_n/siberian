<?php

namespace app\services;


use unyii2\imap\Mailbox;
use yii\base\Exception;

class ImapMailService
{
    private $mailbox;
    private $path;

    public function __construct(Mailbox $mailBox, $pathForMail)
    {
        $this->mailbox = $mailBox;
        $this->path = $pathForMail;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function downloadLetters()
    {
        $mailsIds = $this->mailbox->searchMailBox();
        if (is_array($mailsIds) && !empty($mailsIds)) {
            foreach ($mailsIds as $id) {
                $date = $this->getMailDate($id);
                $this->mailbox->saveMail($id, $this->path.'/eml_'.$date.'_'.$id.'.eml');
            }

            return true;
        } else {
            throw new Exception('На почте нет писем.');
        }
    }

    private function getMailDate($id)
    {
        $mail = $this->mailbox->getMail($id);
        $fullDate = $mail->date;
        $arrDate = explode(' ', $fullDate);
        $date = $arrDate[0];

        return $date;
    }
}