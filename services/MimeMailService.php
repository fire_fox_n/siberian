<?php

namespace app\services;


use app\entities\User;
use app\repositories\NotFoundException;
use app\repositories\PostRepository;
use app\repositories\ProjectRepository;
use app\repositories\UserRepository;
use dosamigos\transliterator\TransliteratorHelper;
use PHPHtmlParser\Exceptions\EmptyCollectionException;
use PhpMimeMailParser\Parser;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use PHPHtmlParser\Dom;

class MimeMailService
{
    private $parser;
    private $emlPath;
    private $userRepo;
    private $signUpService;
    private $dom;
    private $postService;
    private $projectService;
    private $postRepository;
    private $projectRepository;

    public function __construct(
        Parser $parser,
        UserRepository $userRepo,
        SignupService $signUpService,
        PostService $postService,
        ProjectService $projectService,
        PostRepository $postRepository,
        ProjectRepository $projectRepository,
        $path
    ) {
        $this->parser = $parser;
        $this->emlPath = $path;
        $this->userRepo = $userRepo;
        $this->signUpService = $signUpService;
        $this->dom = new Dom;
        $this->postService = $postService;
        $this->projectService = $projectService;
        $this->postRepository = $postRepository;
        $this->projectRepository = $projectRepository;
    }

//    public function __destruct()
//    {
//        FileHelper::removeDirectory(\Yii::getAlias('@app/web/attachments'));
//    }

    public function parse()
    {
        $files = FileHelper::findFiles($this->emlPath);

        $generator = $this->getDocuments($files);
        foreach ($generator as $letterPath) {
            $fullName = $this->getFileName($letterPath);
            $fileName = $this->getFileNameWithoutExtension($fullName);

            $attachDir = $this->emlPath.'/'.$fileName.'/';
            FileHelper::createDirectory($attachDir);

            $this->parser->setStream(fopen($this->emlPath.'/'.$fullName, "r"));
            $this->parser->saveAttachments($attachDir);
        }
    }

    private function getDocuments($files)
    {
        foreach ($files as $file) {
            yield $file;
        }
    }

    private function getFileName($filePath)
    {
        $arr = explode('/', $filePath);
        $fullName = array_pop($arr);

        return $fullName;
    }

    private function getFileNameWithoutExtension($fullName)
    {
        $arr = explode('.', $fullName);

        return $arr[0];
    }

    public function viewSubFolders()
    {
        $skip = array('.', '..');
        $folders = scandir($this->emlPath);
        $folderGenerator = $this->getDocuments($folders);
        foreach($folderGenerator as $folder) {
            if(!in_array($folder, $skip)) {
                if (is_dir($this->emlPath.'/'.$folder)) {
                    echo $folder.'<br>';

                    $subFiles = FileHelper::findFiles($this->emlPath.'/'.$folder);
                    $dateStr = $this->getDateStringFromName($folder);

                    $generator = $this->getDocuments($subFiles);
                    foreach ($generator as $attachment) {
                        echo $attachment.'<br>';
                        $handle = fopen($attachment, "r");
                        $this->parser->setStream($handle);

                        $to = $this->parser->getHeader('to');
                        $user = $this->getUser($to);

                        $html = $this->parser->getMessageBody('html');

                        $this->dom->load($html);
                        $titles = $this->dom->find('td.project-name');
                        /** @var Dom\InnerNode $table */
                        $table = $this->dom->find('table', 0);
                        $trs = $table->find('tr');

                        $projectName = '';
                        $author = '';
                        $post = '';
                        /** @var Dom\InnerNode $tr */
                        $trGenerator = $this->getDocuments($trs);
                        foreach ($trGenerator as $tr) {
                            /** @var Dom\AbstractNode $td */
                            $td = $tr->find('td');
                            $class = $td->getAttribute('class');
                            if ($class == 'project-name') {
                                $projectName = $td->innerHtml();
                                echo 'Название проекта: '.$projectName.'<br>';
                            } else {
                                try {
                                    $innerTable = $td->find('table');
                                    $innerTrs = $innerTable->find('tr');

                                    /** @var Dom\InnerNode $innerTr */
                                    foreach ($innerTrs as $innerTr) {
                                        $innerTd = $innerTr->find('td');
                                        $innerClass = $innerTd->getAttribute('class');
                                        if ($innerClass == 'post-text') {
                                            $post = $innerTd->innerHtml();
                                            echo 'Пост текст: '. $innerTd->innerHtml();
                                            $this->makePost($dateStr, $user, $author, $projectName, $post);
                                            echo '<br>=======<br><br>';
                                        } else {
                                            $authorDom = $innerTd->find('a.user-name-link');
                                            $author = $authorDom->innerHtml();
                                            echo 'Автор: '.$authorDom->innerHtml();
                                            echo '<br>';
                                        }
                                    }
                                } catch (EmptyCollectionException $e) {
                                    //какой-нибудь код
                                }

                            }

                        }

//                        echo $html;

                        echo '==============<br>';
//                        die;
                    }
                    echo '=====================<br><br>';
                }
            }
        }
//        die;
    }

    private function makePost($dateStr, User $forUser, $author, $projectName, $post)
    {
        $author = trim($author);
        $arrAuthor = explode(' ', $author);
        $dateObject = $this->getDateObject($dateStr);
        $dateForSave = $this->getDateForSave($dateObject);
        $dateStr = $dateForSave->format('Y-m-d');

        //get project
        try {
            $project = $this->projectRepository->getByName(trim($projectName));
            echo '<br>Проект найден: '.trim($projectName).'<br>';
        } catch (NotFoundException $e) {
            echo '<br>Проект НЕ найден: '.trim($projectName).'<br>';
            $project = $this->projectService->create(trim($projectName));
        }

        try {
            $objAuthor = $this->userRepo->getByFullName($author);
        } catch (NotFoundException $e) {
            $objAuthor = $this->signUpService->signup(
                TransliteratorHelper::process($arrAuthor[0]),
                mb_strtolower(TransliteratorHelper::process($arrAuthor[0])).'@siberian.pro',
                'qwerty123'
            );

            $profile = $this->signUpService->createProfile($objAuthor->id, $arrAuthor[1], $arrAuthor[0]);
        }

        try {
            $objPost = $this->postRepository->getByAll($objAuthor->id, $project->id, $post, $dateStr);
            $this->postService->edit($objPost->id, $objAuthor, $project, $forUser, $dateStr, $dateStr);
        } catch (NotFoundException $e) {
            $objPost = $this->postService->create($objAuthor, $project, $forUser, $post, $dateStr, $dateStr);
        }

    }

    /**
     * @param \DateTime $dateTime
     *
     * @return \DateTime
     */
    private function getDateForSave(\DateTime $dateTime)
    {
        $numberDay = $dateTime->format('w');
        if ($numberDay == 1) {
            $dateForSave = $dateTime->sub(new \DateInterval('P2D'));
        } else {
            $dateForSave = $dateTime->sub(new \DateInterval('P1D'));
        }

        return $dateForSave;
    }

    private function getDateStringFromName($dirName)
    {
        $arr = explode('_', $dirName);

        return $arr[1];
    }

    private function getDateObject($str)
    {
        $dateObj = \DateTime::createFromFormat('Y-m-d', $str);

        return $dateObj;
    }

    private function getUser($email)
    {
        try {
            $user = $this->userRepo->getByEmail(trim($email));
        } catch (NotFoundHttpException $e) {
            $user = $this->signUpService->signup($email, $email, 'qwerty666');
        }

        return $user;
    }
}