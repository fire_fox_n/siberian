<?php

namespace app\repositories;


use app\entities\Project;

class ProjectRepository
{
    public function get($id)
    {
        $id = intval($id);
        $project = Project::findOne($id);

        if (!$project) {
            throw new NotFoundException('Проект не найден');
        }

        return $project;
    }

    /**
     * @param string $name
     *
     * @return Project
     */
    public function getByName($name)
    {
        return $this->getBy(['name' => $name]);
    }

    public function remove(Project $project)
    {
        if (!$project->delete()) {
            throw new \RuntimeException('Ошибка при удалении проекта.');
        }
    }

    /**
     * @param Project $project
     *
     * @throws \RuntimeException
     * @return void
     */
    public function save(Project $project)
    {
        if (!$project->save()) {
            throw new \RuntimeException('Ошибка сохранения проекта.');
        }
    }

    /**
     * @param array $condition
     *
     * @return Project
     * @throws NotFoundException
     */
    private function getBy(array $condition)
    {
        if (!$user = Project::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Проект не найден.');
        }

        return $user;
    }
}