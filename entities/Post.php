<?php

namespace app\entities;

use Yii;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $author_id
 * @property integer $project_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $type
 * @property string $body
 * @property integer $is_deleted
 *
 * @property User $author
 * @property Project $project
 * @property UsersToPost[] $usersToPosts
 * @property User[] $users
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['usersToPosts'],
            ]
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'project_id'], 'required'],
            [['author_id', 'project_id', 'created_at', 'updated_at', 'is_deleted'], 'integer'],
            [['body'], 'string'],
            [['type'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'project_id' => 'Project ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'body' => 'Body',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public static function create($author_id, $project_id, $body, Dates $dates)
    {
        $post = new static();
        $post->author_id = $author_id;
        $post->project_id = $project_id;
        $post->body = $body;
        $post->created_at = $dates->getTimestampCreatedAt();
        $post->updated_at = $dates->getTimestampUpdatedAt();

        return $post;
    }

    public function edit($author_id, $project_id, Dates $dates)
    {
        $this->author_id = $author_id;
        $this->project_id = $project_id;
        $this->created_at = $dates->getTimestampCreatedAt();
        $this->updated_at = $dates->getTimestampUpdatedAt();
    }

    public function assignUser($id)
    {
        $assignments = $this->usersToPosts;
        foreach ($assignments as $assignment) {
            if ($assignment->isForUser($id)) {
                return;
            }
        }
        $assignments[] = UsersToPost::create($id);
        $this->usersToPosts = $assignments;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersToPosts()
    {
        return $this->hasMany(UsersToPost::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('users_to_post', ['post_id' => 'id']);
    }
}
