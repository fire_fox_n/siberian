<?php

namespace app\services;


use app\entities\Dates;
use app\entities\Post;
use app\entities\Project;
use app\entities\User;
use app\repositories\PostRepository;

class PostService
{
    private $postRepo;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepo = $postRepository;
    }

    /**
     * @param User    $author
     * @param Project $project
     * @param string  $createdAtStr
     * @param string  $updatedAtStr
     *
     * @return Project
     */
    public function create(User $author, Project $project, User $user, $body, $createdAtStr, $updatedAtStr)
    {
        $objCreatedAt = $this->getDateObject($createdAtStr);
        $objUpdatedAt = $this->getDateObject($updatedAtStr);

        $post = Post::create(
            $author->id,
            $project->id,
            $body,
            new Dates($objCreatedAt, $objUpdatedAt)
        );

        $post->assignUser($user->id);

        $this->postRepo->save($post);

        return $post;
    }

    public function edit($id, User $author, Project $project, User $user, $createdAtStr, $updatedAtStr)
    {
        $post = $this->postRepo->get($id);

        $objCreatedAt = $this->getDateObject($createdAtStr);
        $objUpdatedAt = $this->getDateObject($updatedAtStr);

        $post->edit(
            $author->id,
            $project->id,
            new Dates($objCreatedAt, $objUpdatedAt)
        );

        $post->assignUser($user->id);

        $this->postRepo->save($post);
    }

    private function getDateObject($str)
    {
        $dateObj = \DateTime::createFromFormat('Y-m-d', $str);

        return $dateObj;
    }
}