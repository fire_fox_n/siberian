<?php
namespace app\entities;


class Dates
{
    /**
     * @var \DateTime
     */
    private $createdAt;
    private $updatedAt;

    public function __construct(\DateTime $createdAt, \DateTime $updatedAt)
    {
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    public function getTimestampCreatedAt()
    {
        return $this->createdAt->getTimestamp();
    }

    public function getTimestampUpdatedAt()
    {
        return $this->updatedAt->getTimestamp();
    }
}