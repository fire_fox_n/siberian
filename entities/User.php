<?php

namespace app\entities;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Post[] $posts
 * @property Profile $profile
 * @property ProjectToUser[] $projectToUsers
 * @property Project[] $projects
 * @property UsersToPost[] $usersToPosts
 * @property Post[] $posts0
 */
class User extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     *
     * @return User
     */
    public static function requestSignup($username, $email, $password)
    {
        $user = new static();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->created_at = time();
        $user->updated_at = $user->created_at;
        $user->status = self::STATUS_ACTIVE;
        $user->generateAuthKey();

        return $user;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    private function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    private function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getPosts()
//    {
//        return $this->hasMany(Post::className(), ['author_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getProfile()
//    {
//        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getProjectToUsers()
//    {
//        return $this->hasMany(ProjectToUser::className(), ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getProjects()
//    {
//        return $this->hasMany(Project::className(), ['id' => 'project_id'])->viaTable('project_to_user', ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getUsersToPosts()
//    {
//        return $this->hasMany(UsersToPost::className(), ['user_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getPosts0()
//    {
//        return $this->hasMany(Post::className(), ['id' => 'post_id'])->viaTable('users_to_post', ['user_id' => 'id']);
//    }
}
