<?php

namespace app\services;


use app\entities\Profile;
use app\entities\User;
use app\repositories\UserRepository;

class SignupService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function signup($username, $email, $password)
    {
        $user = User::requestSignup(
            $username,
            $email,
            $password
        );

        $this->userRepository->save($user);

        return $user;
    }

    public function createProfile($userId, $firstName, $lastName)
    {
        $profile = Profile::create($userId, $firstName, $lastName);

        $this->userRepository->saveProfile($profile);

        return $profile;
    }
}