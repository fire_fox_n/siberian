<?php
/**
 * Created by PhpStorm.
 * User: natali
 * Date: 04.09.17
 * Time: 11:59
 */

namespace app\repositories;


use app\entities\Post;

class PostRepository
{
    /**
     * @param $id
     *
     * @return Post
     */
    public function get($id)
    {
        $id = intval($id);
        $post = Post::findOne($id);

        if (!$post) {
            throw new NotFoundException('Пост не найден');
        }

        return $post;
    }

    /**
     * @param string $name
     *
     * @return Post
     */
    public function getByName($name)
    {
        return $this->getBy(['name' => $name]);
    }

    public function getByAll($author_id, $project_id, $post, $createdAtStr)
    {
        $objCreatedAt = $this->getDateObject($createdAtStr);

        return $this->getBy([
            'author_id' => $author_id,
            'project_id' => $project_id,
            'created_at' => $objCreatedAt->getTimestamp(),
            'body' => $post
        ]);
    }

    public function remove(Post $post)
    {
        if (!$post->delete()) {
            throw new \RuntimeException('Ошибка при удалении поста.');
        }
    }

    /**
     * @param Post $post
     *
     * @throws \RuntimeException
     * @return void
     */
    public function save(Post $post)
    {
        if (!$post->save()) {
            throw new \RuntimeException('Ошибка сохранения поста.');
        }
    }

    /**
     * @param array $condition
     *
     * @return Post
     * @throws NotFoundException
     */
    private function getBy(array $condition)
    {
        if (!$user = Post::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Пост не найден.');
        }

        return $user;
    }

    private function getDateObject($str)
    {
        $dateObj = \DateTime::createFromFormat('Y-m-d', $str);

        return $dateObj;
    }
}