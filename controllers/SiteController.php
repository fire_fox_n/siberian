<?php

namespace app\controllers;

use app\repositories\PostRepository;
use app\repositories\ProjectRepository;
use app\repositories\UserRepository;
use app\services\ImapMailService;
use app\services\MimeMailService;
use app\services\PostService;
use app\services\ProjectService;
use app\services\SignupService;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpMimeMailParser\Parser;
use unyii2\imap\Mailbox;

class SiteController extends Controller
{
    private $userRepo;
    private $signUpService;
    private $postService;
    private $projectService;
    private $postRepository;
    private $projectRepository;

    public function __construct(
        $id,
        $module,
        UserRepository $userRepo,
        SignupService $signUp,
        PostService $postService,
        ProjectService $projectService,
        PostRepository $postRepository,
        ProjectRepository $projectRepository,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->userRepo = $userRepo;
        $this->signUpService = $signUp;
        $this->postService = $postService;
        $this->projectService = $projectService;
        $this->postRepository = $postRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $flagRes = true;
        FileHelper::createDirectory(Yii::getAlias('@app/web/attachments'));
        $mailbox = new Mailbox(yii::$app->imap->connection);
        $imapMailService = new ImapMailService($mailbox, Yii::getAlias('@app/web/attachments'));

        try {
            $flagRes = $imapMailService->downloadLetters();
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        if ($flagRes) {
            $parser = new Parser();
            $mimeMailService = new MimeMailService(
                $parser,
                $this->userRepo,
                $this->signUpService,
                $this->postService,
                $this->projectService,
                $this->postRepository,
                $this->projectRepository,
                Yii::getAlias('@app/web/attachments')
            );
            $mimeMailService->parse();
            $mimeMailService->viewSubFolders();
        }


        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
